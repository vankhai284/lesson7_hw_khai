﻿List<int> myList = new List<int>(); 
for(int i = 1; i <= 10; i++)
        {
    Console.Write("Nhap so nguyen thu " + i + ": ");
    int num = int.Parse(Console.ReadLine());
    myList.Add(num);
}
// ds moi gom cac so le lon hon 5
List<int> filteredList = myList.Where(x => x % 2 == 1 && x > 5).ToList();

// sap xep giam dan
filteredList.Sort((a, b) => b.CompareTo(a));
Console.WriteLine("Danh sach sau khi loc va sap xep:");
foreach (int num in filteredList)
{
    Console.WriteLine(num);
}